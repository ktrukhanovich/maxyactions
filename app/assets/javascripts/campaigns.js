function updatePriorities() {
    var alphabet = ["Primary", "Secondary A", "Secondary B", "Secondary C", "Secondary D", "Secondary E", "Secondary F", "Secondary G", "Secondary H", "Secondary I", "Secondary J", "Secondary K", "Secondary L", "Secondary M", "Secondary N", "Secondary O", "Secondary P", "Secondary Q", "Secondary R", "Secondary S", "Secondary T", "Secondary U", "Secondary V", "Secondary W", "Secondary X", "Secondary Y", "Secondary Z"];
    $('.priority').each(function (i, el) {
        $(el).text(alphabet[i] + ':');
    });

    $('.priorityInput').each(function (i, el) {
        $(el).val(alphabet[i]);
    });
}

function addAction(content) {
    var new_id = new Date().getTime();
    var id_regexp = /new_action/g;

    $('.actions').append(content.replace(id_regexp, new_id));
    updatePriorities()
}

function addAttribute(target, content) {
    var new_id = new Date().getTime();
    var regexp = /new_attribute/g;
    $(target).before(content.replace(regexp, new_id));
}

function addElement(content) {
    var new_id = new Date().getTime();
    var id_regexp = /new_element/g;

    $('.elements').append(content.replace(id_regexp, new_id));
}

function addVariant(target, content) {
    var new_id = new Date().getTime();
    var id_regexp = /new_variant/g;

    $(target).before(content.replace(id_regexp, new_id));
}

$(document).on('click', '#mobPlat input', function () {
    if ($(this).is(':checked')) {
        if ($('#mobPlatforms').is(':animated')) {
            return false
        }
        $('#mobPlatforms').slideDown();
    }
    else {
        $('#mobPlatforms').slideUp();
    }
});

$(document).on('click', 'img.iconClose', function () {
    var parent = $(this).parents('div.action');
    parent.children('.destroy').val('1');
    parent.children('.panel').remove();
    parent.hide().removeClass('action priority').addClass('action_removed')
    updatePriorities();
})

$(document).on('click', 'img.iconCloseEl', function () {
    var parent = $(this).parents('div.element');
    parent.children('.destroy').val('1');
    parent.hide().removeClass('element').addClass('element_removed')
})

$(document).on('ready page:change', function () {
    $('.actions').on('keyup', 'input.Spec', function () {
        mainVal = $(this).val();
        $(this).next('input').val(mainVal);
    });
});

function getCheckedStatuses(table, prefix) {

    var allSelectors = table.find('.stat');
    var prefixLen = prefix.length;
    var obj = {};

    for (var i = 0; i < allSelectors.length; i++) {
        if (allSelectors[i].value.length > 0) {

            var sec_Symbol = allSelectors[i].id.indexOf('_', prefixLen);

            element = allSelectors[i].id.substring(prefixLen, allSelectors[i].id.indexOf('_', prefixLen));
            browser = allSelectors[i].id.substring(sec_Symbol + 1);

            if (!obj[element]) {
                obj[element] = {}
            }
            obj[element][browser] = allSelectors[i].value;
        }
    }
    return obj
}

$(document).on('click', ".nav", function (event) {
    var target = $(event.target);
    if (target.hasClass('elemTab')) {
        $('#elemTab').addClass('active');
        $('#actTab').removeClass('active');
        $('.tabActions').addClass('hidden');
        $('.tabElements').removeClass('hidden');
    } else if (target.hasClass('actTab')) {
        $('#elemTab').removeClass('active');
        $('#actTab').addClass('active');
        $('.tabActions').removeClass('hidden');
        $('.tabElements').addClass('hidden');
    }
})

