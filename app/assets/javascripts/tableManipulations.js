function initStatuses() {
    var json = $('#statuses_json').text();
    if (!json)
        return;

    var statuses = JSON.parse(json)

    setStatuses('actS', statuses['actions']);
    setStatuses('elemS', statuses['elements']);
}

function initDesignations() {
    $(".designation").click(function () {
        $(this).parent().find(".designation").removeClass("dsg-checked");
        $(this).addClass('dsg-checked');
    });
}

function setStatuses(idPrefix, statuses) {
    jQuery.each(statuses, function (attr_id, browser_statuses) {
        jQuery.each(browser_statuses, function (browser_id, val) {
            $('#' + idPrefix + '_' + attr_id + '_' + browser_id).val(val)
        })
    });
}

function saveCheckedStatuses() {
    var statusesAct = getCheckedStatuses($('.tabActions').find('.status-table'), 'actS_');
    var statusesElem = getCheckedStatuses($('.tabElements').find('.status-table'), 'elemS_');
    var allStatObj = {'actions': statusesAct, 'elements': statusesElem}
    var json = JSON.stringify(allStatObj);

    var campaignId = $("#campaign_id").text();
    $.post("/campaigns/" + campaignId + "/statuses", {statuses: json})
}

function markAll() {
    var closestTable = $(this).closest('.status-table');
    var designationValue = $(this).parent().find(".dsg-checked").val();
    closestTable.find('.stat').val(designationValue);
    statusChanged(closestTable);
}

function checkCell() {
    var cell = $(this);
    var closestTable = cell.closest('.status-table');
    if (cell.val() === '' || cell.val() !== closestTable.find(".dsg-checked").val()) {
        cell.val(closestTable.find(".dsg-checked").val());
        statusChanged(closestTable);
    }
}

function statusChanged(statTable) {
    $(statTable).each(function (i, statTable) {
        statTable = $(statTable);

        var progressBar = statTable.find('.progress-bar, .progress-bar-green');
        updateProgressBar(progressBar, calculateProgress(statTable));

        saveCheckedStatuses();
    });
}

function updateProgressBar(prBar, val) {
    var spanWithText = prBar.find('span');
    prBar.attr('style', 'width:' + Math.round(val) + '%');
    spanWithText.attr('style', 'width:' + Math.round(val) + '%');
    spanWithText.text(Math.round(val) + '% Complete');
    if (Math.round(val) === 0) {
        spanWithText.css('color', 'black')
    } else {
        spanWithText.css('color', '#ffffff')
    }
    if (Math.round(val) > 90) {
        prBar.removeClass('progress-bar');
        prBar.addClass('progress-bar-green');
    } else {
        prBar.removeClass('progress-bar-green');
        prBar.addClass('progress-bar');
    }
}

function calculateProgress(statTable) {
    const doneValues = ['+', '*', '~'];

    var stats = statTable.find('.stat');
    var totalCount = stats.length;

    var doneCount = $.grep(stats,function (stat, i) {
        return $.inArray($(stat).val(), doneValues) > -1;
    }).length;

    return 100 * doneCount / totalCount
}

$(document).on('page:change', function () {
    if ($('div.editButtons').length) {

        initStatuses();

        initDesignations();

        statusChanged($('.status-table'));

        $("[rel=tooltip]").tooltip({ placement: 'right'});

        $('.markAll').click(function () {
            markAll.call(this)
        });

        $('.stat').click(
            function () {
                checkCell.call(this)
            }
        );
    }
});
