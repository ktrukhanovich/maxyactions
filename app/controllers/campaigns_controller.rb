class CampaignsController < ApplicationController
  before_action :authenticate_user!
  load_and_authorize_resource only: [:show, :edit, :update, :destroy, :update_statuses]

  skip_before_action :verify_authenticity_token, only: [:update_statuses]

  def update_statuses
    @campaign.statuses_json = params[:statuses]
    respond_to do |format|
      if @campaign.save
        format.json { head :no_content }
      else
        format.json { render json: @campaign.errors, status: :unprocessable_entity }
      end
    end
  end

  # GET /campaigns
  # GET /campaigns.json
  def index
    @campaigns = Campaign.all
  end

  # GET /campaigns/1
  # GET /campaigns/1.json
  def show
    @browsers = @campaign.browsers

    respond_to do |format|
      format.html
      format.json
      format.xls
    end
  end

  # GET /campaigns/new
  def new
    @campaign = Campaign.new

    action = @campaign.actions.build priority: 'Primary'
    action.m_attributes.build name: '-'

    element = @campaign.elements.build
    element.e_variants.build

  end

  # GET /campaigns/1/edit
  def edit
  end

  # POST /campaigns
  # POST /campaigns.json
  def create
    @campaign = Campaign.new(campaign_params)
    update_browsers
    @campaign.owner = current_user

    respond_to do |format|
      if @campaign.save
        format.html { redirect_to @campaign, notice: 'Campaign was successfully created.' }
        format.json { render action: 'show', status: :created, location: @campaign }
      else
        format.html { render action: 'new' }
        format.json { render json: @campaign.errors, status: :unprocessable_entity }
      end
    end
  end

  def update_browsers
    browser_ids = params[:campaign][:checked_browsers]
    @campaign.browsers.clear
    unless browser_ids.nil?
      browser_ids.each do |browser_id|
        @campaign.browsers << Browser.find_by_id(browser_id)
      end
    end
  end

  # PATCH/PUT /campaigns/1
  # PATCH/PUT /campaigns/1.json
  def update
    @campaign.assign_attributes campaign_params
    update_browsers

    respond_to do |format|
      if @campaign.save
        format.html { redirect_to @campaign, notice: 'Campaign was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @campaign.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /campaigns/1
  # DELETE /campaigns/1.json
  def destroy
    @campaign.destroy
    respond_to do |format|
      format.html { redirect_to campaigns_url }
      format.json { head :no_content }
    end
  end

  private
  # Use callbacks to share common setup or constraints between actions.
  def set_campaign
    @campaign = Campaign.find(params[:id])
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def campaign_params
    params.require(:campaign).permit(:name, actions_attributes: [:id, :name, :ui_name, :priority, :_destroy, m_attributes_attributes: [:id, :name]], elements_attributes: [:id, :name, :ui_name, :_destroy, e_variants_attributes: [:id, :name]])
  end
end
