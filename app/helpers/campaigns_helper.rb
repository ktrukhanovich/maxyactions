module CampaignsHelper

  def link_to_add_action(name, f, html_options={})
    new_action = Action.new
    new_action.m_attributes.build :name => '-'
    new_action = f.fields_for(:actions, new_action, :child_index => 'new_action') do |builder|
      render('campaigns/action', :f => builder, action: new_action)
    end
    link_to_function(name, "addAction(\"#{escape_javascript(new_action)}\")", html_options)
  end

  def link_to_add_attribute(name, f, html_options={})
    new_attribute = MAttribute.new
    attribute = f.fields_for(:m_attributes, new_attribute, :child_index => 'new_attribute') do |builder|
      render('campaigns/attribute', :f => builder)
    end
    link_to_function name, "addAttribute(this, \"#{escape_javascript(attribute)}\")", html_options
  end

  def link_to_add_element(name, f, html_options={})
    new_element = Element.new
    new_element.e_variants.build
    new_element = f.fields_for(:elements, new_element, :child_index => 'new_element') do |builder|
      render('campaigns/element', :f => builder, element: new_element)
    end
    link_to_function(name, "addElement(\"#{escape_javascript(new_element)}\")", html_options)
  end

  def link_to_add_variant(name, f, html_options={})
    new_variant = EVariant.new
    variant = f.fields_for(:e_variants, new_variant, :child_index => 'new_variant') do |builder|
      render('campaigns/variant', :f => builder)
    end
    link_to_function name, "addVariant(this, \"#{escape_javascript(variant)}\")", html_options
  end

end
