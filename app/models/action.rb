class Action < ActiveRecord::Base
  belongs_to :campaign
  has_many :m_attributes, :dependent => :destroy, :order => 'name asc'

  accepts_nested_attributes_for :m_attributes, :reject_if => lambda { |a| a[:name].blank? }, :allow_destroy => true
end
