class Campaign < ActiveRecord::Base
  belongs_to :owner, class_name: 'User'

  has_many :actions, :dependent => :destroy, :order => 'priority asc'
  has_many :elements, :dependent => :destroy, :order => 'name asc'
  has_and_belongs_to_many :browsers

  accepts_nested_attributes_for :actions, :reject_if => lambda { |a| a[:name].blank? }, :allow_destroy => true
  accepts_nested_attributes_for :elements, :reject_if => lambda { |a| a[:name].blank? }, :allow_destroy => true
end
