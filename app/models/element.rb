class Element < ActiveRecord::Base
  belongs_to :campaign
  has_many :e_variants, :dependent => :destroy, :order => 'name asc'

  accepts_nested_attributes_for :e_variants, :reject_if => lambda { |a| a[:name].blank? }, :allow_destroy => true
end