json.array!(@browsers) do |browser|
  json.extract! browser, :id, :name, :category
  json.url browser_url(browser, format: :json)
end
