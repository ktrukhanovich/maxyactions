class CreateActions < ActiveRecord::Migration
  def change
    create_table :actions do |t|
      t.string :name
      t.references :campaign, index: true

      t.timestamps
    end
  end
end
