class RenameAttributeToMAttribute < ActiveRecord::Migration
  def change
    rename_table :attributes, :m_attributes
  end
end
