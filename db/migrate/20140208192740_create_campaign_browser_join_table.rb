class CreateCampaignBrowserJoinTable < ActiveRecord::Migration
  def change
    create_join_table :campaigns, :browsers do |t|
      t.index :campaign_id
      t.index :browser_id
    end
  end
end
