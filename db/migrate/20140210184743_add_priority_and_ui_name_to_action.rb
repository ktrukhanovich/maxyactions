class AddPriorityAndUiNameToAction < ActiveRecord::Migration
  def change
    add_column :actions, :priority, :string
    add_column :actions, :ui_name, :string
  end
end
