class AddStatusesToCampaign < ActiveRecord::Migration
  def change
    add_column :campaigns, :statuses_json, :string
  end
end
