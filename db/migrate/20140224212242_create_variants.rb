class CreateVariants < ActiveRecord::Migration
  def change
    create_table :e_variants do |t|
      t.string :name
      t.references :element, index: true

      t.timestamps
    end
  end
end

