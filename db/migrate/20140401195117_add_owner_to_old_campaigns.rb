class AddOwnerToOldCampaigns < ActiveRecord::Migration
  def up
    execute 'update campaigns set owner_id = 1 where owner_id is null'
  end

  def down
  end
end
