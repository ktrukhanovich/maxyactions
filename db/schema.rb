# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20140401195117) do

  create_table "actions", force: true do |t|
    t.string "name"
    t.integer "campaign_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string "priority"
    t.string "ui_name"
  end

  add_index "actions", ["campaign_id"], name: "index_actions_on_campaign_id"

  create_table "browsers", force: true do |t|
    t.string "name"
    t.string "category"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer "campaign_id"
  end

  add_index "browsers", ["campaign_id"], name: "index_browsers_on_campaign_id"

  create_table "browsers_campaigns", id: false, force: true do |t|
    t.integer "campaign_id", null: false
    t.integer "browser_id", null: false
  end

  add_index "browsers_campaigns", ["browser_id"], name: "index_browsers_campaigns_on_browser_id"
  add_index "browsers_campaigns", ["campaign_id"], name: "index_browsers_campaigns_on_campaign_id"

  create_table "campaigns", force: true do |t|
    t.string "name"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string "statuses_json"
    t.integer "owner_id"
  end

  add_index "campaigns", ["owner_id"], name: "index_campaigns_on_owner_id"

  create_table "e_variants", force: true do |t|
    t.string "name"
    t.integer "element_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "e_variants", ["element_id"], name: "index_e_variants_on_element_id"

  create_table "elements", force: true do |t|
    t.string "name"
    t.integer "campaign_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "elements", ["campaign_id"], name: "index_elements_on_campaign_id"

  create_table "m_attributes", force: true do |t|
    t.string "name"
    t.integer "action_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "m_attributes", ["action_id"], name: "index_m_attributes_on_action_id"

  create_table "users", force: true do |t|
    t.string "email", default: "", null: false
    t.string "encrypted_password", default: "", null: false
    t.string "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "users", ["email"], name: "index_users_on_email", unique: true
  add_index "users", ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true

end
