# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)
Browser.delete_all
Browser.create(name: 'Chrome', category: "Windows")
Browser.create(name: 'Firefox', category: "Windows")
Browser.create(name: 'Internet Explorer 8', category: "Windows")
Browser.create(name: 'Internet Explorer 9', category: "Windows")
Browser.create(name: 'Internet Explorer 10', category: "Windows")
Browser.create(name: 'Internet Explorer 11', category: "Windows")
Browser.create(name: 'Chrome', category: "Mac")
Browser.create(name: 'Firefox', category: "Mac")
Browser.create(name: 'Safari 5', category: "Mac")
Browser.create(name: 'Safari 6', category: "Mac")
Browser.create(name: 'Safari 7', category: "Mac")
Browser.create(name: 'Safari 5', category: "iOS")
Browser.create(name: 'Safari 6', category: "iOS")
Browser.create(name: 'Safari 7', category: "iOS")
Browser.create(name: 'Chrome', category: "iOS")
Browser.create(name: 'Chrome', category: "Android")
Browser.create(name: 'Native Browser', category: "Android")
Browser.create(name: 'Internet Explorer', category: "Windows Phone")
