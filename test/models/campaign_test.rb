require 'test_helper'

class CampaignTest < ActiveSupport::TestCase
  test 'Create nested actions and attributes' do
    param = {name: 'Test Campaign', actions_attributes: {'0' => {name: 'Test Action', m_attributes_attributes: {'0' => {name: 'Attribute'}}}}}

    campaign = Campaign.new(param)
    campaign.save

    assert_equal 'Test Campaign', campaign.name
    assert_equal 1, campaign.actions.count
    assert_equal 'Test Action', campaign.actions[0].name
    assert_equal 1, campaign.actions[0].m_attributes.size
  end
end
